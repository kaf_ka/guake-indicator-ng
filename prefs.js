/*
 * Kodi Connector
 * An extensionto control your Kodi/XBMC boxes remotely with GNOME Shell
 *
 * Copyright (C) 2018
 *     Lorenzo Carbonell <lorenzo.carbonell.cerezo@gmail.com>,
 * https://www.atareao.es
 *
 * This file is part of Kodi Connector
 *
 * Kodi Connector is free software: you can redistribute it and/or modify
 * it under the terms of the GNU General Public License as published by
 * the Free Software Foundation, either version 3 of the License, or
 * (at your option) any later version.
 *
 * Kodi Connector is distributed in the hope that it will be useful,
 * but WITHOUT ANY WARRANTY; without even the implied warranty of
 * MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 * GNU General Public License for more details.
 *
 * You should have received a copy of the GNU General Public License
 * along with the extension.
 * If not, see <http://www.gnu.org/licenses/>.
  */

const GLib = imports.gi.GLib;
const GObject = imports.gi.GObject;
const Gio = imports.gi.Gio;
const Gtk = imports.gi.Gtk;

const Extension = imports.misc.extensionUtils.getCurrentExtension();

var folders = {
    ICONS: "icons",
    SCHEMA: "schemas",
    LOCALE: "locale"
}

var settings = _get_settings('org.gnome.shell.extensions.guakeindicatorng');
var key_name = "hide-icon-when-inactive";

const _ = imports.gettext.domain(Extension.metadata['gettext-domain']).gettext;

class AboutWidget extends Gtk.Grid{
    constructor() {
        super({
            margin_bottom: 18,
            row_spacing: 8,
            hexpand: true,
            halign: Gtk.Align.CENTER,
            orientation: Gtk.Orientation.VERTICAL
        });
        // this.get_style_context().add_class("frame");

        let programbox = new Gtk.Box({
            can_focus: false,
            margin_top: 32,
            margin_bottom: 15,
            orientation: Gtk.Orientation.VERTICAL
        });
        // programbox.get_style_context().add_class("frame");
        this.add(programbox);

        let aboutIcon = new Gtk.Image({
            icon_name: "utilities-terminal",
            pixel_size: 128
        });
        programbox.add(aboutIcon);

        let aboutName = new Gtk.Label({
            label: "<b>" + Extension.metadata.name.toString() + "</b>",
            use_markup: true
        });
        programbox.add(aboutName);

        let aboutVersion = new Gtk.Label({ label: _("Version") + ": " + Extension.metadata.version.toString() });
        programbox.add(aboutVersion);

        let aboutDescription = new Gtk.Label({
            label:  Extension.metadata.description
        });
        programbox.add(aboutDescription);

        let aboutWebsite = new Gtk.Label({
            label: '<a href="%s">%s</a>'.format(
                Extension.metadata.url,
                Extension.metadata.url
            ),
            use_markup: true,
            margin_top: 16
        });
        programbox.add(aboutWebsite);

        // let aboutCopyright = new Gtk.Label({
        //     label: "<small>" + _('Copyleft 2018') + "</small>",
        //     use_markup: true
        // });
        // this.add(aboutCopyright);

        let box = new Gtk.Box({
            can_focus: false,
            margin_left: 0,
            margin_right: 0,
            margin_top: 16,
            // margin_bottom: 24,
            orientation: Gtk.Orientation.VERTICAL
        });
        // box.get_style_context().add_class("frame");
        this.add(box);

        let label = new Gtk.Label({
            can_focus: false,
            margin_bottom: 12,
            margin_start: 3,
            xalign: 0,
            use_markup: true,
            label: "<b>" + _("Preferences") + "</b>"
        });
        box.pack_start(label, false, true, 0);

        let frame = new Gtk.Frame({
            can_focus: false,
            margin_bottom: 32,
            hexpand: true,
            shadow_type: Gtk.ShadowType.IN
        });
        box.add(frame);

        let listbox = new Gtk.ListBox({
            can_focus: false,
            hexpand: true,
            activate_on_single_click: true,
            selection_mode: Gtk.SelectionMode.NONE,
            width_request: 240
        });
        frame.add(listbox);
        listbox.set_header_func((row, before) => {
            if (before) {
                row.set_header(
                    new Gtk.Separator({ orientation: Gtk.Orientation.HORIZONTAL })
                );
            }
        });

        let row = new Gtk.ListBoxRow({
            can_focus: false,
            activatable: false,
            selectable: false,
            height_request: 56
        });
        listbox.add(row);

        let grid = new Gtk.Grid({
            can_focus: false,
            column_spacing: 12,
            margin_left: 12,
            margin_top: 8,
            margin_bottom: 8,
            margin_right: 12,
            vexpand: true,
            valign: Gtk.Align.FILL
        });
        row.add(grid);

        let key = settings.settings_schema.get_key(key_name);
        let summary = new Gtk.Label({
            can_focus: false,
            xalign: 0,
            hexpand: true,
            valign: Gtk.Align.CENTER,
            vexpand: true,
            label: key.get_summary(),
            use_markup: true
        });
        grid.attach(summary, 0, 0, 1, 1);

        let description = new Gtk.Label({
            xalign: 0,
            hexpand: true,
            valign: Gtk.Align.CENTER,
            vexpand: true,
            label: key.get_description(),
            use_markup: true,
            wrap: true
        });
        description.get_style_context().add_class("dim-label");
        grid.attach(description, 0, 1, 1, 1);

        let switcher = new Gtk.Switch({
            can_focus: true,
            halign: Gtk.Align.END,
            valign: Gtk.Align.CENTER,
            visible: true
        });
        settings.bind(key_name, switcher, "active", Gio.SettingsBindFlags.DEFAULT);
        grid.attach(switcher, 1, 0, 1, 2);

        let aboutLicense = new Gtk.Label({
            label: "<small>" +
            "This program is free software: you can redistribute it and/or modify" + "\n" +
            "it under the terms of the GNU General Public License as published by" + "\n" +
            "the Free Software Foundation, either version 3 of the License, or" + "\n" +
            "(at your option) any later version." + "\n\n" +
            "This program is distributed in the hope that it will be useful," + "\n" +
            "but WITHOUT ANY WARRANTY; without even the implied warranty of" + "\n" +
            "MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the" + "\n" +
            "GNU General Public License for more details." + "\n\n" +
            "You should have received a copy of the GNU General Public License" + "\n" +
            "along with this program.\n  If not, see <a href=\"https://www.gnu.org/licenses/\">https://www.gnu.org/licenses/</a>." + "\n" +
            "</small>",
            margin_top: 24,
            use_markup: true,
            // width_chars: 80,
            justify: Gtk.Justification.CENTER
        });
        // aboutLicense.get_style_context().add_class("frame");
        aboutLicense.set_padding(15, 0);
        box.add(aboutLicense);
    }
}

function init () {
    imports.gettext.bindtextdomain(
        Extension.metadata['gettext-domain'],
        Extension.dir.get_child(folders.LOCALE).get_path()
    );
}

function buildPrefsWidget() {
    let wrsp = new Gtk.Stack({
        transition_type: Gtk.StackTransitionType.SLIDE_LEFT_RIGHT
    });

    let switcher = new Gtk.StackSwitcher({
        halign: Gtk.Align.CENTER,
        stack: wrsp
    });
    switcher.show_all();

    let aboutPage = new Gtk.ScrolledWindow({
        can_focus: true,
        hscrollbar_policy: Gtk.PolicyType.NEVER,
        valign: Gtk.Align.FILL,
        vexpand: true,
        vscrollbar_policy: Gtk.PolicyType.NEVER
    });
    wrsp.add_titled(aboutPage, "about", _("About"));

    let box = new Gtk.Box({
        can_focus: false,
        margin_left: 72,
        margin_right: 72,
        margin_top: 18,
        margin_bottom: 32,
        orientation: Gtk.Orientation.VERTICAL
    });
    aboutPage.add(box);

    box.add(new AboutWidget());

    GLib.timeout_add(GLib.PRIORITY_DEFAULT, 0, () => {
        let prefsWindow = wrsp.get_toplevel()
        prefsWindow.get_titlebar().custom_title = switcher;
        return false;
    });

    wrsp.show_all();
    return wrsp;
}

function _get_settings(schema, path=null) {
    let schemaSource = Gio.SettingsSchemaSource.new_from_directory(
        Extension.dir.get_child(folders.SCHEMA).get_path(),
        Gio.SettingsSchemaSource.get_default(),
        false
    );

    let schemaObj = schemaSource.lookup(schema, true);

    let preferences = { settings_schema: schemaObj };
    if (path != null) preferences.path = path;

    return new Gio.Settings(preferences);
}
