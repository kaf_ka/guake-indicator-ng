/**
    This program is free software: you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation, either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program.  If not, see <http://www.gnu.org/licenses/>.
**/

const Lang = imports.lang;
const Mainloop = imports.mainloop;

const St = imports.gi.St;
const Gio = imports.gi.Gio;
const GLib = imports.gi.GLib;
const Shell = imports.gi.Shell;
const Clutter = imports.gi.Clutter;

const Main = imports.ui.main;
const Tweener = imports.ui.tweener;
const PanelMenu = imports.ui.panelMenu;
const PopupMenu = imports.ui.popupMenu;

const Util = imports.misc.util;

const Extension = imports.misc.extensionUtils.getCurrentExtension();

const GUAKE_SETTINGS_SCHEMA = 'apps.guake.general';
const GUAKE_SETTINGS_HEIGHT = 'window-height';

const EXT_SETTINGS_SCHEMA = 'org.gnome.shell.extensions.guakeindicatorng';
const EXT_SETTINGS_AUTO_HIDE = 'hide-icon-when-inactive';

const UPDATE_INTERVAL = 1;
const DBUS_INTERVAL = 5;


// <interface name="org.freedesktop.DBus.Introspectable"> \
// <method name="Introspect"> \
//     <arg type="s" direction="out" name="something"/> \
// </method> \
// </interface> \

const RemoteControlIface = '<node> \
<interface name="org.guake3.RemoteControl"> \
<method name="quit"/> \
<method name="show_about"/> \
<method name="rename_current_tab"> \
    <arg type="s" direction="in" name="name"/> \
</method> \
<method name="execute_command"> \
    <arg type="s" direction="in" name="command"/> \
</method> \
<method name="show_prefs"/> \
<method name="show"/> \
<method name="hide"/> \
<method name="show_hide"/> \
<method name="execute_command"> \
    <arg type="s" direction="in" name="command"/> \
</method> \
<method name="get_gtktab_name"> \
    <arg type="i" direction="in" name="tab_index"/> \
    <arg type="s" direction="out" name="tab_name"/> \
</method> \
<method name="get_tab_name"> \
    <arg type="i" direction="in" name="tab_index"/> \
    <arg type="s" direction="out" name="tab_name"/> \
</method> \
<method name="get_selected_tab"> \
    <arg type="i" direction="out" name="tab_index"/> \
</method> \
<method name="get_tab_count"> \
    <arg type="i" direction="out" name="tab_count"/> \
</method> \
<method name="add_tab"> \
    <arg type="s" direction="in" name="directory"/> \
    <arg type="s" direction="out" name="something"/> \
</method> \
<method name="select_tab"> \
    <arg type="i" direction="in" name="tab_index"/> \
    <arg type="i" direction="out" name="something"/> \
</method> \
</interface> \
</node>';


var pidof_cmd;
var guake_cmd;

var indicator;

var gauke_settings;
var extention_settings;

var RemoteControlWrapper;

var dbus_proxy;
var watch_event;
var update_event

var folders = {
    ICONS: "icons",
    SCHEMA: "schemas",
    LOCALE: "locale"
}

var _ = _get_gettext();

function init() {
    imports.gettext.bindtextdomain(
        Extension.metadata['gettext-domain'],
        Extension.dir.get_child(folders.LOCALE).get_path()
    );

    pidof_cmd = GLib.find_program_in_path("pidof");
    guake_cmd = GLib.find_program_in_path("guake");

    gauke_settings = _get_settings(GUAKE_SETTINGS_SCHEMA);
    extention_settings = _get_settings(EXT_SETTINGS_SCHEMA);

    RemoteControlWrapper = Gio.DBusProxy.makeProxyWrapper(RemoteControlIface);
    dbus_proxy = new RemoteControlWrapper(
        Gio.DBus.session,
        'org.guake3.RemoteControl',
        '/org/guake3/RemoteControl'
    );
}

function enable() {
    indicator = new Indicator();
    Main.panel.addToStatusArea('guake-indicator-ng', indicator, 0, 0);

    if (extention_settings.get_boolean(EXT_SETTINGS_AUTO_HIDE)) watch_guake();
    extention_settings.connect("changed::" + EXT_SETTINGS_AUTO_HIDE, Lang.bind(this, () => {
        if (extention_settings.get_boolean(EXT_SETTINGS_AUTO_HIDE)) watch_guake();
        else {
            if (watch_event != null) {
                Mainloop.source_remove(watch_event);
                watch_event = null;
            }
            indicator.setVisible(true);
        }
    }));
}

function disable() {
    if (update_event != null) {
        Mainloop.source_remove(update_event);
        update_event = null;
    }

    if (watch_event != null) {
        Mainloop.source_remove(watch_event);
        watch_event = null;
    }

    if (indicator != null) {
        indicator.destroy();
        indicator = null;
    }
}

function start_guake() {
    let application = Shell.AppSystem.get_default().lookup_app('guake.desktop');
    application.activate();
}

function watch_guake() {
    let argv = [pidof_cmd, "-x", guake_cmd];
    let [ok, output, errors, status] = GLib.spawn_sync(
        null, argv, null, GLib.SpawnFlags.DEFAULT, null
    );

    let pid = -1;
    if (ok && output != null) {
        let pidstr = "";
        for (let i=0; i<output.length; i++)
            pidstr += String.fromCharCode(output[i]);
        pid = parseInt(pidstr);
    }

    indicator.setVisible((pid > -1));

    if (watch_event != null) Mainloop.source_remove(watch_event);
    watch_event = GLib.timeout_add_seconds(
        0, UPDATE_INTERVAL, Lang.bind(this, watch_guake)
    );
}

function _get_gettext() {
    return imports.gettext.domain(Extension.metadata['gettext-domain']).gettext;
}

function _get_settings(schema, path=null) {
    let schemaSource = Gio.SettingsSchemaSource.new_from_directory(
        Extension.dir.get_child(folders.SCHEMA).get_path(),
        Gio.SettingsSchemaSource.get_default(),
        false
    );

    let schemaObj = schemaSource.lookup(schema, true);

    let preferences = { settings_schema: schemaObj };
    if (path != null) preferences.path = path;

    return new Gio.Settings(preferences);
}

var Indicator = new Lang.Class({
    Name: 'Indicator',
    Extends: PanelMenu.Button,

    _visible: true,
    _panel_button: null,

    _init () {
        this.parent(0, "guake-indicator-ng");

        let panel_icon = new St.Icon({ icon_name: 'utilities-terminal-symbolic',
                                 style_class: 'system-status-icon' });

        this._panel_button = new St.Button({ style_class: 'panel-button',
                                     reactive: true,
                                     can_focus: true,
                                     x_fill: true,
                                     y_fill: false,
                                     track_hover: true });
        this._panel_button.set_child(panel_icon);

        this._panel_button.connect(
            'button-press-event',
            Lang.bind(this, this._activate)
        );

        this.actor.add_actor(this._panel_button);

        this.create_popmenu();
    },

    create_popmenu () {
        this._panel_button.connect(
            'scroll-event',
            Lang.bind(this, this._scroll)
        );

        this._create_tab_list("application-menu-symbolic", _("Tabs"));

        this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        // this._create_menuitem("terminal-symbolic", "Toggle", Lang.bind(this, () => {
        this._create_menuitem(null, _("Toggle"), Lang.bind(this, () => {
            try {
                dbus_proxy.show_hideSync();
            } catch (err) {
                start_guake();

                GLib.timeout_add_seconds(0, 1, Lang.bind(this, () => {
                    dbus_proxy.showSync();
                }));
            }
        }));
        // this._create_menuitem("tab-new-symbolic", "New Tab", Lang.bind(this, () => {
        this._create_menuitem(null, _("New Tab"), Lang.bind(this, () => {
            try {
                dbus_proxy.add_tabSync("./~");
                dbus_proxy.showSync();
            } catch (err) {
                start_guake();

                GLib.timeout_add_seconds(0, 1, Lang.bind(this, () => {
                    dbus_proxy.add_tabSync("./~");
                    dbus_proxy.showSync();
                }));
            }
        }));
        // this._create_menuitem("tab-new-background-symbolic", "New Tab as root", Lang.bind(this, () => {
        this._create_menuitem(null, _("New Tab as root"), Lang.bind(this, () => {
            try {
                dbus_proxy.add_tabSync("./~");
                dbus_proxy.showSync();
                dbus_proxy.execute_commandSync("sudo su -");
            } catch (err) {
                start_guake();

                GLib.timeout_add_seconds(0, 1, Lang.bind(this, () => {
                    dbus_proxy.add_tabSync("./~");
                    dbus_proxy.showSync();
                    dbus_proxy.execute_commandSync("sudo su -");
                }));
            }
        }));

		this.menu.addMenuItem(new PopupMenu.PopupSeparatorMenuItem());

        this._create_menuitem("preferences-system-symbolic", _("Prefereces"), Lang.bind(this, () => {
            try {
                dbus_proxy.show_prefsSync();
            } catch (err) {
                start_guake();

                GLib.timeout_add_seconds(0, 1, Lang.bind(this, () => {
                    dbus_proxy.show_prefsSync();
                }));
            }
        }));
    },

    _create_menuitem (icon_name, label_text, clicked) {
        let box = new St.BoxLayout({
            vertical: false,
            style_class: 'menuitem-box'
        });

        if (icon_name != null && icon_name != "") {
            let icon = new St.Icon({
                icon_name: icon_name,
                style_class: 'popup-menu-icon'
            });
            box.add_child(icon);
        }

        let label = new St.Label({
            text: label_text,
            style: 'margin-left: 15px;'
        });
        box.add_child(label);

        let menu_item = new PopupMenu.PopupBaseMenuItem();
        menu_item.actor.add(box, null);
        menu_item.connect('activate', clicked);

        this.menu.addMenuItem(menu_item);
    },

    _create_tab_list (lacel_icon, label_text) {
        this.tab_menu = new GuakeTabMenu(lacel_icon, label_text);

        this.menu.connect(
                'open-state-changed',
                Lang.bind(this, function(menu, open) {
                    if (open)
                        this.tab_menu.startUpdateInterval();
                    else
                        this.tab_menu.stopUpdateInterval();
                }));

        this.menu.addMenuItem(this.tab_menu);
    },

    _activate (source, event) {
        try {
            if (event.get_button() == 1) dbus_proxy.show_hideSync();
        } catch (err) {
            start_guake();

            GLib.timeout_add_seconds(0, 1, Lang.bind(this, () => {
                dbus_proxy.showSync();
            }));
        }
    },

    _scroll (source, event) {
        let direction = event.get_scroll_direction();
        let height = gauke_settings.get_int(GUAKE_SETTINGS_HEIGHT);

        if (direction == Clutter.ScrollDirection.UP) {
            height = height - 3;
        } else if (direction == Clutter.ScrollDirection.DOWN) {
            height = height + 3;
        }

        try {
            dbus_proxy.showSync();
            gauke_settings.set_int(GUAKE_SETTINGS_HEIGHT, height);
        } catch (err) {}
    },

    setVisible (visible) {
        if (this._visible == visible) return;
        this._visible = visible;

        if (visible) {
            this.actor.show();
            this.actor.opacity = 255;
            this.actor.set_width(-1);
        } else {
            this.actor.hide();
            this.actor.opacity = 0;
            this.actor.set_width(0);
        }
    },

	destroy () {
    	this.parent();
	}
});

var GuakeTabMenu = new Lang.Class({
    Name: 'GuakeTabButton',
    Extends: PopupMenu.PopupSubMenuMenuItem,

    _guake_tabs: [],
    _update_event: null,

    _init (icon_name, title) {
        this.parent(title, true);

        this.icon.icon_name = icon_name;
        this.label.txt = title;

        this._guake_tabs = [];
        this.create_tabs();
    },

    create_tabs () {
        try {
            let [actual_tabs_length, err] = dbus_proxy.get_tab_countSync();

            for (var i = this._guake_tabs.length; i < actual_tabs_length; i++) {
                let new_tab = new GuakeTabButton(i);
                this._guake_tabs.push(new_tab);
                this.menu.addMenuItem(new_tab);
            }

            for (var i = this._guake_tabs.length; i > actual_tabs_length; i--) {
                let closed_tab = this._guake_tabs.pop();
                closed_tab.destroy();
            }

            for (var i = 0; i < this._guake_tabs.length; i++) {
                this._guake_tabs[i].refresh();
            }
        } catch (err) {
            for (var i = 0; i < this._guake_tabs.length; i++) {
                let closed_tab = this._guake_tabs.pop();
                closed_tab.destroy();
            }
        }

        if (update_event != null) {
            this.stopUpdateInterval();
            this.startUpdateInterval();
        }
    },

    startUpdateInterval () {
        if (update_event == null) {
            update_event = GLib.timeout_add_seconds(
                0, UPDATE_INTERVAL, Lang.bind(this, this.create_tabs)
            );
        }
    },

    stopUpdateInterval () {
        if (update_event != null) {
	        Mainloop.source_remove(update_event);
            update_event = null;
        }
    },

	destroy () {
        this.stopUpdateInterval();
    	this.parent();
	}
});

var GuakeTabButton = new Lang.Class({
    Name: 'GuakeTabButton',
    Extends: PopupMenu.PopupBaseMenuItem,

    _tab_index: 0,
    _label: null,

    _init (tab_index) {
        this.parent();

        this._tab_index = tab_index;

        let icon = new St.Icon({
            icon_name: "gtk-execute",
            style_class: 'popup-menu-icon'
        });

        this.label = new St.Label({
            // text: label_text,
            style: 'margin-left: 15px;'
        });

        let box = new St.BoxLayout({
            vertical: false,
            style_class: 'menuitem-box'
        });
        box.add_child(icon);
        box.add_child(this.label);

        let button = new St.Button({ reactive: true,
                                      can_focus: true,
                                      x_fill: true,
                                      y_fill: false,
                                      track_hover: true });

        button.connect('button-press-event', Lang.bind(this, function(actor, event) {
            try {
                dbus_proxy.select_tabSync(parseInt(this._tab_index));
                    // let [sfdgsdf, err] = dbus_proxy.select_tabSync(parseInt(this._tab_index));
                dbus_proxy.showSync();
            } catch (err) {}
        }));
        button.add_actor(box);

        this.actor.add(button, { expand: true });
    },

    refresh () {
        try {
            let [tab_name, err] = dbus_proxy.get_gtktab_nameSync(this._tab_index);
            this.label.text = tab_name;
        } catch (err) {}
    },

	destroy () {
    	this.parent();
	}
});
