# guake-indicator-ng
This is a indicator for "guake". Some additional features have also been added.

- You can toggle the visibility of the terminal window with a left mouse click.
- The middle and right mouse button opens a menu. The menu contains an overview
    of the different tabs, a toggle button, new tabs and preferences.
- By scrolling on the icon you can easily change the height of the terminal window.

(It is possible to hide the extension when "guake" is terminated.)
